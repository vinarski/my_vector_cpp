/*
Bla *bb = new Bla ();
This makes two following operations:
1. allocates memory for storage of the object of size_ of Bla
   and returns a pointer to this chunk of raw memory
2. calls (in this case) (default) constructor of Bla and places
   the resulting object within the previously allocated memory
   using placement new.
Those individual steps you can do manually:

1. void *b   = operator new (size_of (Bla));
2. Bla *bla = new (b) Bla ();  //new (this) Kshared_ptr (ptrNew);

For the sake of the method push_back (T obj) the second step needs to use
the copy constructor as it receives an existing  object to be placed
into the pre-allocated memory.

void *ptr = operator new (size_of(Bla));
Bla *bla = new(ptr) Bla ();
Bla *bla2 = new Bla (10);
void *ptr2 = operator new (size_of(Bla));
Bla *bla3 = new(ptr2) Bla(*bla2);

https://eli.thegreenplace.net/2011/02/17/the-many-faces-of-operator-new-in-c

*/

#include <memory>
#include <string.h>
#include <stdexcept>

#define MAX_capacity_ 100000
#define VECTOR_DELTA 1000

template <typename T>
class Kvector
{

private:
    unsigned size_;
    unsigned capacity_;
    unsigned delta;
    T *arr;

    /* private methods */

    void setResizeDelta (unsigned newDelta)
    {
        if (newDelta < MAX_capacity_ / 2 && newDelta > delta)
            delta = newDelta;
    }

    void resize (unsigned newcapacity_)
    {
        if (newcapacity_ <= capacity_
            || newcapacity_ > MAX_capacity_)
            return;

         //new raw memory
        T *tmpArr = static_cast<T*>(operator new (sizeof (T) * newcapacity_));

        for (int i = 0; i < size_; i++)
        {
            new (tmpArr + i) T(arr[i]); //placement new + copy constructor
            (arr + i)->~T(); //destroy just copied obj within the old memory
        }

        if (arr != NULL)
            operator delete (arr); //destroy the old memory without calling destructors

        capacity_ = newcapacity_;
        arr = tmpArr;
    }

public:

    //CTOR
    Kvector ()
    :size_(0), capacity_(0), delta (VECTOR_DELTA), arr (NULL)
    {}

    //copy CTOR
    Kvector (const Kvector& other)
    :size_(other.size_), capacity_(other.capacity_), delta (other.delta)
    {
        //new raw memory
        arr = static_cast<T*>(operator new (sizeof (T) * capacity_));
        for (int i = 0; i < size_; i++)
            new (arr + i) T (other[i]);
    }

    //DTOR
    ~Kvector ()
    {
        //iretare over the whole memory occupied by objects of type T
        //calling destructor on each object
        for (int i = 0; i < size_; i++)
            (arr + i)->~T();
            //alternatives:
            // arr[i].~T();
            // (*(arr + i)).~T();

        if (arr != NULL)
            operator delete (arr); //destroy the whole previously allocated memory
    }

    unsigned size () const {return size_;}

    unsigned capacity () const {return capacity_;}

    bool isEmpty () const {return size_ == 0;}

    void reserve (unsigned userDefiniedcapacity_)
    {
        resize (userDefiniedcapacity_);
    }

    void push_back (const T &element)
    {
        if (size_ == capacity_)
            resize (capacity_ + delta);

        //placement new due to already existing memory
        //creating the object of type T into this chunk of memory
        //by calling the copy constructor
        new (arr + size_) T(element);
        ++size_;
    }

    void pop_back ()
    {
        if (isEmpty ())
            throw std::out_of_range ("ERROR: the vector is empty!");
        (arr + size_-- - 1)->~T();
    }

    T delete_back ()
    {
        if (isEmpty ())
            throw std::out_of_range ("ERROR: the vector is empty!");
        T tmp = arr [size_ - 1];
        (arr + size_-- - 1)->~T();
        return tmp;
    }

    T& at (int index)
    {
        if (index >= size_)
        {
            std::string msg;
            msg.reserve (256);
            msg += "ERROR: Index ";
            msg += std::to_string (index);
            msg += " is out of range! The size is ";
            msg += std::to_string (size_);
            msg += "\n the capacity is ";
            msg += std::to_string (capacity_);

            throw std::out_of_range (msg);
        }

        return arr[index];
    }

    const T& at (int index) const
    {
        if (index >= size_)
        {
            std::string msg;
            msg.reserve (256);
            msg += "Index ";
            msg += std::to_string (index);
            msg += " is out of range! The size is ";
            msg += std::to_string (size_);
            msg += "\n the capacity is ";
            msg += std::to_string (capacity_);

            throw std::out_of_range (msg);
        }

        return arr[index];
    }

    //subscript operator overload
    T& operator[] (int index) {return at (index);}

    const T& operator[] (int index) const {return at (index);}

    Kvector& operator= (const Kvector& other)
    {
        if(this == &other) return *this;  //handle self assignment

        if (this->capacity_ < other.capacity_)
        {
            this->~Kvector();
            new (this) Kvector (other);
            return *this;
        }

        this->clear ();

        size_ = other.size_;
        for (int i = 0; i < size_; i++)
            new (arr + i) T (other[i]);

        return *this;
    }

    T& back ()
    {
        if (isEmpty ())
            throw std::out_of_range ("The vector is empty!");

        return arr[size_ - 1];
    }

    T& front ()
    {
        if (isEmpty ())
            throw std::out_of_range ("The vector is empty!");

        return arr[0];
    }

    //nested class
    class iterator
    {

    private:
        Kvector *thisVector;
        unsigned currIteratorIndex;

    public:

        //traits - makes the iterator STL compatible by mapping
        //internal names to uniform names
        //and by naming the iterator category
        typedef T value_type;
        typedef std::ptrdiff_t difference_type;
        typedef T * pointer;
        typedef T& reference;
        typedef std::random_access_iterator_tag iterator_category;

        //CTOR
        iterator (Kvector *v = NULL, bool isAfterLast = false):
            thisVector (v), currIteratorIndex (isAfterLast ? v->size_ : 0) {}

        //copy CTOR
        iterator (const iterator& other):
            thisVector (other.thisVector),
            currIteratorIndex (other.currIteratorIndex) {}

        //DTOR
        ~iterator () {}

        unsigned getCurrIteratorIndex () const {return currIteratorIndex;}

        //operator overloads
        iterator& operator= (const iterator& right)
        {
            if (this == &right)
                return *this; // handle self assignment

            //this->~iterator();
            //new (this) iterator (right);
            //better (more efficient):
            thisVector = right.thisVector;
            currIteratorIndex = right.currIteratorIndex;
            return *this;
        }

        bool operator== (const iterator& other) const
        {
            if (this == &other) //handle self comparison
                return true;

            return
                (thisVector == other.thisVector //same vector instance?
                 && currIteratorIndex == other.currIteratorIndex);
        }

        bool operator!= (const iterator& other) const
        {
            return !(*this == other);
        }

        // Prefix version
        iterator& operator++ ()
        {
            ++currIteratorIndex;
            return *this;
        }

        // Prefix version
        iterator& operator-- ()
        {
            --currIteratorIndex;
            return *this;
        }

        // Postfix version
        iterator operator++ (int)
        {
            iterator tmp (*this);
            ++currIteratorIndex;
            return tmp;
        }

        // Postfix version
        iterator operator-- (int)
        {
            iterator tmp (*this);
            --currIteratorIndex;
            return tmp;
        }

        iterator operator+ (int i)
        {
            iterator newIter (*this);
            unsigned newCurrIteratorIndex = currIteratorIndex + i;
            if (newCurrIteratorIndex >= thisVector->size_)
                newCurrIteratorIndex = thisVector->size_;

            newIter.currIteratorIndex = newCurrIteratorIndex;
            return newIter;
        }

        iterator operator- (int i)
        {
            iterator newIter (*this);
            unsigned newCurrIteratorIndex = currIteratorIndex - i;
            if (newCurrIteratorIndex < 0)
                newCurrIteratorIndex = 0;

            newIter.currIteratorIndex = newCurrIteratorIndex;
            return newIter;
        }

        //determine the (index-)difference between two iterators
        int operator- (const iterator &rhs)
        {
            return  currIteratorIndex - rhs.currIteratorIndex;
        }

        iterator &operator+= (int i)
        {
            unsigned newCurrIteratorIndex = currIteratorIndex + i;
            if (newCurrIteratorIndex >= thisVector->size_)
                newCurrIteratorIndex = thisVector->size_;

            this.currIteratorIndex = newCurrIteratorIndex;
            return *this;
        }

        iterator &operator-= (int i)
        {
            unsigned newCurrIteratorIndex = currIteratorIndex - i;
            if (newCurrIteratorIndex < 0)
                newCurrIteratorIndex = 0;

            this.currIteratorIndex = newCurrIteratorIndex;
            return *this;
        }

        T& operator* ()
        {
            return thisVector->arr[currIteratorIndex];
        }

        T *operator-> ()
        {
            return (thisVector->arr + currIteratorIndex);
        }
    };

    iterator begin ()
    {
        return iterator (this);
    }

    iterator end ()
    {
        return iterator (this, true);
    }

    void clear () //leaves the capacity() of the vector unchanged
    {
        if (isEmpty ())
            return;

        for (int i = 0; i < size_; i++)
            (arr + i)->~T();
        size_ = 0;
    }

    iterator insert (iterator pos, const T& value) //inserts value before pos
    {
        if (size_ == capacity_)
            resize (capacity_ + delta);

        if (pos == this->end())
        {
            push_back (value);
            return pos;
        }

        //shift values to the right until (including) pos

        //last element needs to be moved by invoking
        //the placement new with the copy CTOR
        //as there is no object to the right of the last one
        push_back (arr[size_ - 1]);

        //the other elements shift to the right
        //by copying and replacing the onces to the right
        //which are already a redundant copy
        int position = pos.getCurrIteratorIndex();
        for (int i = size_ - 3; i >= position; i--)
            arr[i + 1] = arr [i];

        arr[position] = value;
        return pos;
    }

};

