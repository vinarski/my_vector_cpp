//#include <vector>
#include <algorithm>
#include <stdio.h>

#include "kvector.h"

#define vector Kvector

class Bla
{
    public:
        void set_value (int value) {this->value = value;}
        int get_value () const {return value;}

        Bla (int value = 0) :value(value)
        {
            printf ("CTOR(). %d %p\n", value, this);
        }

        Bla (const Bla &bla) : value(bla.value)
        {
            printf ("Copy CTOR(). %d %p\n", value, this);
        }

        ~Bla () {printf ("DTOR(). %d %p\n", value, this);}

        Bla& operator= (const Bla &right)
        {
            printf ("operator=  %d %p\n", right.value, this);
            if (this == &right)
                return *this;

            value = right.value;
                return *this;
        }

        bool operator== (const Bla& other) const
        {
            if (this == &other) //handle self comparison
                return true;
            return this->value == other.value;
        }

    private:
        int value;
};


void print_vector (vector<Bla> &v)
{
    vector<Bla>::iterator iter;
    for (iter = v.begin(); iter != v.end(); ++ iter)
        printf ("%d ", iter->get_value());
    printf ("\n");
}


int main (void)
{
    vector<Bla> v;
    v.reserve (10);

    Bla bla1 = 10;
    Bla bla2 = 20;
    Bla bla3 = 30;
    Bla bla4 = 40;
    Bla bla0 = 0;

    printf ("back 1.\n");
    v.push_back (bla1);
    printf ("back 2.\n");
    v.push_back (bla2);
    printf ("back 3.\n");
    v.push_back (bla3);
    printf ("back 4.\n");
    v.push_back (bla4);

    printf ("front 1.\n");

    v.insert (v.begin (), bla0);
    printf ("front 1 finish.\n");

    print_vector (v);

    vector<Bla>::iterator iter;

    iter = std::find (v.begin(), v.end(), bla3);
    if (iter != v.end())
        printf ("Found: %d\n", iter->get_value());
    else
        printf ("Not found. %d\n", bla3.get_value());

    Bla bla5 = 50;
    iter = std::find (v.begin(), v.end(), bla5);
    if (iter != v.end())
        printf ("Found: %d\n", iter->get_value());
    else
        printf ("Not found %d.\n", bla5.get_value());

    return 0;
}

