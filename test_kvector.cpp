#include <vector>
#include <stdio.h>
#include <iostream>
#include "kvector.h"

class Bla
{
    public:
        void set_value (int value) {this->value = value;}

        Bla (int value = 0) :value(value)
        {
            printf ("CTOR(). %d %p\n", value, this);
        }

        Bla (const Bla &bla) : value(bla.value)
        {
            printf ("Copy CTOR(). %d %p\n", value, this);
        }

        ~Bla () {printf ("DTOR(). %d %p\n", value, this);}

        Bla & operator= (const Bla &right)
        {
            printf ("operator=  %d %p\n", right.value, this);
            if (this != &right) //handle self-assignment
                value = right.value;
            return *this;
        }

        friend std::ostream& operator<< (std::ostream& os, const Bla& b)
        {
            os << b.value;
            return os;
        }

    private:
        int value;
};


int main (void)
{
    Kvector<Bla> v;
    v.reserve (10);

    Bla bla1 = 10;
    Bla bla2 = 20;
    Bla bla3 = 30;
    Bla bla4 = 40;
    Bla bla99 = 99;

    printf ("back 1.\n");
    v.push_back (bla1);
    printf ("back 2.\n");
    v.push_back (bla2);
    printf ("back 3.\n");
    v.push_back (bla3);
    printf ("back 4.\n");
    v.push_back (bla4);

    std::cout << "vector: [ ";
    for (Kvector<Bla>::iterator it = v.begin ();
        it != v.end (); ++it)
        std::cout << *it << ' ';
    std::cout << "]" << std::endl;

    std::cout << (v.isEmpty () ? "the vector is empty." : "the vector is NOT empty") << std::endl;
    std::cout << "size: " << v.size () << "  capacity: " << v.capacity () << std::endl;
    std::cout << "back: " << v.back () << std::endl;
    std::cout << "front: " << v.front () << std::endl;
    std::cout << "at (2): " << v.at (2) << std::endl;

    std::cout << " *** test exception operator[] Index is out of range *** " << std::endl;
    std::cout << " *** v.at (123) *** " << std::endl;
    try
    {
        std::cout << v.at (123) << std::endl;
    }   catch (const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }

    std::cout << "INSERT bla99 (value of 99) into pos 2 (third elem) " << std::endl;
    Kvector<Bla>::iterator iter = v.begin ();
    iter++;
    v.insert (iter+1, bla99);
    std::cout << "Done with INSERT bla99 (value of 99) into pos 2 (third elem) " << std::endl;

    std::cout << "vector: [ ";
    for (Kvector<Bla>::iterator it = v.begin ();
        it != v.end (); ++it)
        std::cout << *it << ' ';
    std::cout << "]" << std::endl;

    std::cout << "v.back(): " << v.back() << "\n";
    std::cout << "v.delete_back(): " << v.delete_back() << " ---end of delete_back\n";

    std::cout << "vector: [ ";
    for (Kvector<Bla>::iterator it = v.begin ();
        it != v.end (); ++it)
        std::cout << *it << ' ';
    std::cout << "]" << std::endl;
    std::cout << "v.back(): " << v.back() << "\n";

    std::cout << "v.delete_back() empty vector: \n";
    Kvector<Bla> empty;
    try
    {
        empty.delete_back();
    }   catch (const std::out_of_range& oor)
        {
            std::cerr << oor.what() << std::endl;
        }

    Kvector<Bla> v2 = v;
    v2 = v;

    std::cout << "v.clear ()" << std::endl;
    v.clear ();
    std::cout << (v.isEmpty () ? "the vector is empty." : "the vector is NOT empty") << std::endl;

    return 0;
}

